package tls

import (
	"context"
	"crypto/tls"
	"net/http"
	"time"

	"github.com/cretz/bine/tor"
)

// DoRequestTLSOverTor make a request over tor
func DoRequestTLSOverTor(host string, ip string) (infoCall, error) {
	t, err := tor.Start(nil, nil)
	if err != nil {
		return infoCall{}, err
	}
	defer t.Close()
	dialCtx, dialCancel := context.WithTimeout(context.Background(), time.Minute)
	defer dialCancel()
	dialer, err := t.Dialer(dialCtx, nil)
	if err != nil {
		return infoCall{}, err
	}
	client := &http.Client{
		Timeout: time.Duration(10) * time.Second,
		Transport: &http.Transport{DialContext: dialer.DialContext,
			MaxIdleConns:        20,
			MaxIdleConnsPerHost: 20,
			TLSClientConfig: &tls.Config{
				ServerName: host,
			},
		},
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	return CommonRequest(client, host, ip)
}
