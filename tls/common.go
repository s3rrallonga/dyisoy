package tls

import (
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type infoCall struct {
	Status     string
	StatusCode int
	Location   string
	Body       string
}

// CommonRequest make a request
func CommonRequest(client *http.Client, host string, ip string) (infoCall, error) {
	var err error

	req, err := http.NewRequest("GET", "https://"+ip, nil)
	if err != nil {
		log.Println(err)
		return infoCall{}, err
	}

	req.Host = host
	resp, err := client.Do(req)
	if err != nil {
		return infoCall{}, err
	}
	defer resp.Body.Close()

	htmlData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return infoCall{}, err
	}

	ic := infoCall{Status: resp.Status, StatusCode: resp.StatusCode}

	switch resp.StatusCode {
	case 301, 302:
		ic.Location = resp.Header.Get("Location")
	default:
		ic.Body = string(htmlData)
	}
	return ic, nil
}
