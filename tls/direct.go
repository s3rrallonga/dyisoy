package tls

import (
	"crypto/tls"
	"net/http"
	"time"
)

// DoRequestTLS make a request
func DoRequestTLS(host string, ip string) (infoCall, error) {

	client := &http.Client{
		Timeout: time.Duration(10) * time.Second,
		Transport: &http.Transport{MaxConnsPerHost: 50,
			MaxIdleConns:        20,
			MaxIdleConnsPerHost: 20,
			TLSClientConfig: &tls.Config{
				ServerName: host,
			},
		},
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	return CommonRequest(client, host, ip)
}
