package main

import (
	dns "dyisoy/dns"
	tls "dyisoy/tls"
	"fmt"
	"os"
	"reflect"

	log "github.com/sirupsen/logrus"
)

func main() {

	argsWithoutProg := os.Args[1:]

	if len(argsWithoutProg) == 0 {
		fmt.Printf("Use:\n\t%s <censored.domain>\n", os.Args[0])
		os.Exit(0)
	}

	goodIP := checkDNS(os.Args[1])
	getDPI(os.Args[1], goodIP)

}

func checkDNS(tld string) string {

	var haveErr = false

	server := "1.1.1.1"

	ipsDNS := dns.GetDirectDNS(tld)
	log.Info(fmt.Sprintf("Direct DNS: %v\n", ipsDNS))

	ipsExternalServer := dns.GetDNSOverSpecificServer(tld, server)
	log.Info(fmt.Sprintf("Specific DNS: %v\n", ipsExternalServer))

	ipsDoH := dns.GetDNSOverHTTPS(tld)
	log.Info(fmt.Sprintf("DNS over HTTPS: %v\n", ipsDoH))

	if !reflect.DeepEqual(ipsDNS, ipsDoH) {
		fmt.Println("[WARNING]: DNS of your ISP is modified.")
		haveErr = true
	}

	if !reflect.DeepEqual(ipsExternalServer, ipsDoH) {
		fmt.Println("[WARNING]: Your ISP modifies your DNS calls.")
		haveErr = true
	}

	if !haveErr {
		fmt.Println("It seems that your ISP does not modify DNS calls.")
	}
	return ipsDoH[0]
}

func getDPI(tld string, ip string) {
	log.Info("Direct TLS")
	icDirect, errDirect := tls.DoRequestTLS(tld, ip)
	if errDirect != nil {
		log.Info(fmt.Sprintf("Error: %v\n", errDirect))
	} else {
		log.Info(fmt.Sprintf("%v\n", icDirect))
	}

	log.Info("TLS Over Tor")
	icTor, errTor := tls.DoRequestTLSOverTor(tld, ip)
	if errTor != nil {
		log.Info(fmt.Sprintf("Error: %v\n", errTor))
	} else {
		log.Info(fmt.Sprintf("%v\n", icTor))
	}

	if icDirect.StatusCode == icTor.StatusCode && icDirect.Location == icTor.Location {
		fmt.Println("It seems your ISP does not do DPI (Deep Packet Inspection)")
	} else {
		log.Warn("Seems that your ISP does Deep Packet Inspection, spies on you!")
		log.Warn("Or the web site you are checking block TOR requests.")
	}
}
