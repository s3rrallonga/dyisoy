package dns

import (
	"context"
	"log"
	"net"
	"os"
	"sort"
	"time"

	"github.com/likexian/doh-go"
	"github.com/likexian/doh-go/dns"
	dnsMiekg "github.com/miekg/dns"
)

// GetDirectDNS Direct DNS call
func GetDirectDNS(tld string) []string {

	var ips []string

	luips, err := net.LookupIP(tld)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	for _, ip := range luips {
		if ip.To4() != nil {
			ips = append(ips, ip.String())
		}
	}
	sort.Strings(ips)
	return ips
}

// GetDNSOverSpecificServer Query DNS to Specific Server
func GetDNSOverSpecificServer(tld string, server string) []string {

	var ips []string

	c := dnsMiekg.Client{}
	m := dnsMiekg.Msg{}
	m.SetQuestion(tld+".", dnsMiekg.TypeA)
	r, _, err := c.Exchange(&m, server+":53")
	if err != nil {
		log.Fatal(err)
	}

	for _, ans := range r.Answer {
		Arecord := ans.(*dnsMiekg.A)
		ips = append(ips, Arecord.A.String())
	}
	sort.Strings(ips)
	return ips
}

// GetDNSOverHTTPS Query DNS over HTTPs
func GetDNSOverHTTPS(tld string) []string {

	var ips []string

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := doh.Use(doh.CloudflareProvider, doh.GoogleProvider)

	rsp, err := c.Query(ctx, dns.Domain(tld), dns.TypeA)
	if err != nil {
		panic(err)
	}

	c.Close()

	answer := rsp.Answer

	for _, a := range answer {
		ips = append(ips, a.Data)
	}
	sort.Strings(ips)
	return ips
}
