module dyisoy

go 1.13

require (
	github.com/cretz/bine v0.1.0
	github.com/likexian/doh-go v0.6.3
	github.com/miekg/dns v1.1.22
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/net v0.0.0-20190923162816-aa69164e4478
)
